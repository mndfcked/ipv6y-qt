DEPTH = ..
include( $${DEPTH}/global.pri )

TARGET = libipv6y

include( $${TARGET_LIB} )

DEFINES += IPV6Y_LIBRARY

HEADERS += libipv6y.h

HEADERS += bouquet.h
HEADERS += command.h
HEADERS += ip6tablescommandfactory.h
HEADERS += ipv6y.h
HEADERS += ipv6yfactory.h
HEADERS += remotecommandfactory.h

SOURCES += bouquet.cpp
SOURCES += command.cpp
SOURCES += ip6tablescommandfactory.cpp
SOURCES += ipv6y.cpp
SOURCES += ipv6yfactory.cpp
SOURCES += remotecommandfactory.cpp

QT += network
QT -= gui

LINK_TARGET = libsocks
LINK_SRC_DIR = $${SOCKSLIB_SRC_DIR}
include( $${LINK_LIBRARY} )
