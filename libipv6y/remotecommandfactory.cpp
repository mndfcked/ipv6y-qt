#include "remotecommandfactory.h"

RemoteCommandFactory::RemoteCommandFactory()
{
}

RemoteCommandFactory::RemoteCommandFactory( const QString &user,
                                            const QString &host )
    : m_user( user ), m_host( host )
{
}

RemoteCommandFactory::~RemoteCommandFactory()
{
}

Command RemoteCommandFactory::getCommand( const QString &program ) const
{
    Command cmd( program );
    if ( !m_host.isNull() ) {
        cmd.setSsh( m_user, m_host );
    }
    return cmd;
}
