#include "ipv6y.h"

#include "bouquet.h"
#include "ip6tablescommandfactory.h"

IPv6y::IPv6y( const Ip6tablesCommandFactory *commandFactory,
              Bouquet *bouquet,
              const QHostAddress &clientAddress,
              const quint16 clientPort,
              QObject *parent )
    : ConnectionFilter( clientAddress, clientPort, parent ),
      m_commandFactory( commandFactory ),
      m_bouquet( bouquet ),
      m_undoCommand( nullptr )
{
    Q_ASSERT( m_commandFactory );
    Q_ASSERT( m_bouquet );
}

IPv6y::~IPv6y()
{
    if ( m_undoCommand ) {
        if ( !m_undoCommand->execute() ) {
            qWarning( "Failed to revert rule: %s",
                      qPrintable( m_undoCommand->errorString() ) );
        }
        delete m_undoCommand;
    }
}

bool IPv6y::isConnectionToAddressAllowed( const QHostAddress &address,
                                          const quint16 port )
{
    Q_UNUSED( port );

    if ( m_bouquet->isEmpty() ) {
        qWarning( "Bouquet is empty." );
        return false;
    }

    // Disable non IPv6 traffic to avoid privacy leaks
    if ( address.protocol() != QAbstractSocket::IPv6Protocol )
        return false;

    // TODO: Handle ports, ie.
    // ip6tables [...] --jump SNAT --to-source [dead::beaf]:port # single port
    // ip6tablse [...] --jump SNAT --to-source [dead::beaf]:port1-port2 # range

    QHostAddress targetAddress = m_bouquet->takeAddress();

    // FIXME: Support UDP
    QAbstractSocket::SocketType protocol = QAbstractSocket::TcpSocket;

    Command cmd = m_commandFactory->getSNATCommand( true,
                                                    protocol,
                                                    address,
                                                    targetAddress );

    if ( !cmd.execute() ) {
        qWarning( "Failed to add rule to table '%s': %s",
                  qPrintable( m_commandFactory->table() ),
                  qPrintable( cmd.errorString() ) );
        return false;
    }

    m_undoCommand = new Command( m_commandFactory->getSNATCommand(
        false, protocol, address, targetAddress ) );

    return true;
}

bool IPv6y::isConnectionToDomainAllowed( const QString &domain,
                                         const quint16 port )
{
    Q_UNUSED( domain );
    Q_UNUSED( port );

    // There is no need to do anything here since isConnectionToAddressAllowed()
    // will be called after the domain got resolved.
    return true;
}
