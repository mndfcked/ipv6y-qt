#include "ipv6yfactory.h"

#include "ipv6y.h"

IPv6yFactory::IPv6yFactory( const Ip6tablesCommandFactory *commandFactory,
                            Bouquet *bouquet,
                            QObject *parent )
    : ConnectionFilterFactory( parent ),
      m_commandFactory( commandFactory ),
      m_bouquet( bouquet )
{
    Q_ASSERT( m_commandFactory );
    Q_ASSERT( m_bouquet );
}

IPv6yFactory::~IPv6yFactory()
{
}

ConnectionFilter *IPv6yFactory::newConnectionFilter(
    const QHostAddress &address, const quint16 port ) const
{
    return new IPv6y( m_commandFactory, m_bouquet, address, port );
}
