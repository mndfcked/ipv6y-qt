#ifndef COMMAND_H
#define COMMAND_H

#include "libipv6y.h"

#include <QtCore/QByteArray>
#include <QtCore/QString>
#include <QtCore/QStringList>

class IPV6YSHARED_EXPORT Command
{
public:
    explicit Command( const QString &program,
                      const QStringList &arguments = QStringList() );

    virtual ~Command();

    void addArgument( const QString &argument );

    QStringList arguments() const;

    QString errorString() const;

    bool execute(); // blocking

    QString program() const;

    bool isSsh() const;

    void setSsh( const QString &user, const QString &host );

private:
    QString m_program;
    QStringList m_arguments;
    QString m_sshUser;
    QString m_sshHost;
    QString m_errorString;
    QByteArray m_stdOut;
    QByteArray m_stdErr;
};

#endif // COMMAND_H
