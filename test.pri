isEmpty( TEST_OUT_DIR ): error( "$${_PRO_FILE_}: TEST_OUT_DIR not set!" )

DESTDIR = $${TEST_OUT_DIR}

# Include in 'make check'
CONFIG *= testcase

# This is required for QSKIP to compile with Qt 5.0.0 to 5.3.0
# See: http://www.kdab.com/porting-from-qt-4-to-qt-5/ (Porting: Skipping Unit Tests)
greaterThan( QT_MAJOR_VERSION, 4 ):lessThan( QT_MINOR_VERSION, 3 ) {
    DEFINES *= Q_COMPILER_VARIADIC_MACROS
}
