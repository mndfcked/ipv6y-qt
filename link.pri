isEmpty( LINK_TARGET ): error( "$${_PRO_FILE_}: LINK_TARGET not set!" )
isEmpty( LINK_SRC_DIR ): error( "$${_PRO_FILE_}: LINK_SRC_DIR not set!" )
isEmpty( LIB_OUT_DIR ): error( "$${_PRO_FILE_}: LIB_OUT_DIR not set!" )
# LINK_CONFIG is optional

# Allow <header.h> style includes
INCLUDEPATH *= $$clean_path( $${LINK_SRC_DIR} )
# Allow <lib/header.h> style includes
INCLUDEPATH *= $$clean_path( $${LINK_SRC_DIR}/.. )

DEPENDPATH *= $$clean_path( $${LINK_SRC_DIR} )

target.depends *= $${LINK_TARGET}

# Build static libraries before building the binaries to avoid linking older
# versions of libraries being linked into the binary.
contains( LINK_CONFIG, static ) {
    win32:!win32-g++ {
        PRE_TARGETDEPS *= $${LIB_OUT_DIR}/$${LINK_TARGET}.lib
    }
    unix|win32-g++ {
        PRE_TARGETDEPS *= $${LIB_OUT_DIR}/lib$${LINK_TARGET}.a
    }
}

LIBS *= -L$${LIB_OUT_DIR}/ -l$${LINK_TARGET}

# Clear configuration
unset( LINK_SRC_DIR )
unset( LINK_TARGET )
unset( LINK_CONFIG )
