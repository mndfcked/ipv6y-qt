DEPTH = ../..
include( $${DEPTH}/global.pri )

TARGET = bouquettest

include( $${TARGET_TEST} )

TEMPLATE = app

CONFIG += console
CONFIG -= app_bundle

HEADERS += bouquettest.h
HEADERS += $${IPV6YLIB_SRC_DIR}/bouquet.h

SOURCES += bouquettest.cpp
SOURCES += $${IPV6YLIB_SRC_DIR}/bouquet.cpp

INCLUDEPATH += $$clean_path( $${IPV6YLIB_SRC_DIR}/.. )

QT += network
QT += testlib
QT -= gui
