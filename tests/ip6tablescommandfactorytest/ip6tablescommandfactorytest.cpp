#include "ip6tablescommandfactorytest.h"

#include <libipv6y/ip6tablescommandfactory.h>

#include <QtTest>

void Ip6tablesCommandFactoryTest::testFlushCommand() const
{
    const QString table = QStringLiteral( "nat" );

    Ip6tablesCommandFactory factory( table );

    Command cmd = factory.getFlushCommand();

    QCOMPARE( cmd.isSsh(), false );
    QCOMPARE( cmd.program(), QStringLiteral( "ip6tables" ) );
    QCOMPARE( cmd.arguments().count(), 3 );
    QCOMPARE( cmd.arguments()[ 0 ], QStringLiteral( "--table" ) );
    QCOMPARE( cmd.arguments()[ 1 ], table );
    QCOMPARE( cmd.arguments()[ 2 ], QStringLiteral( "--flush" ) );
}

void Ip6tablesCommandFactoryTest::testRemoteFlushCommand() const
{
    const QString table = QStringLiteral( "nat" );
    const QString sshUser = QStringLiteral( "user" );
    const QString sshHost = QStringLiteral( "host" );

    Ip6tablesCommandFactory factory( table, sshUser, sshHost );

    Command cmd = factory.getFlushCommand();

    QCOMPARE( cmd.isSsh(), true );
    QCOMPARE( cmd.program(), QStringLiteral( "ssh" ) );
    QCOMPARE( cmd.arguments().count(), 2 );
    QCOMPARE( cmd.arguments()[ 0 ],
              QStringLiteral( "%1@%2" ).arg( sshUser ).arg( sshHost ) );
    QCOMPARE( cmd.arguments()[ 1 ],
              QStringLiteral( "ip6tables --table %1 --flush" ).arg( table ) );
}

QTEST_GUILESS_MAIN( Ip6tablesCommandFactoryTest );
