DEPTH = .
include( $${DEPTH}/global.pri )

TEMPLATE = subdirs

SUBDIRS += libsocks
SUBDIRS += libipv6y
SUBDIRS += ipv6y
SUBDIRS += tests

libipv6y.depends += libsocks

ipv6y.depends += libipv6y

OTHER_FILES += .clang-format
OTHER_FILES += README.md
